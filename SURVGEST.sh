#!/bin/bash
cp build/* .
test -f FiniFini && rm FiniFini
test -f PasOptim && rm PasOptim
test -f FinOptim && rm FinOptim
test -f PARAMETR/reglage.txt && rm PARAMETR/reglage.txt
test -f PARAMETR/OUITRI.TXT && rm PARAMETR/OUITRI.TXT
test -f Temporai.re/etape && rm Temporai.re/etape
test -f Temporai.re/result0 && rm Temporai.re/result0;
test -f Suivopti/suivi && rm Suivopti/suivi
test -f Temporai.re/dichot1 && rm Temporai.re/dichot1
test -f Temporai.re/dichot2 && rm Temporai.re/dichot2
test -f Temporai.re/dichot3 && rm Temporai.re/dichot3
test -f Temporai.re/dichot4 && rm Temporai.re/dichot4


while [ ! -f FinOptim ]
do
    ./REGLAGE
    test -f PasOptim && break 
    ./VGEST
done

rm PARAMETR/reglage.txt
./ENDE
