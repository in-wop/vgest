# VGEST

VGEST is software developped in Pascal by Jean-Claude Bader [^Bader2016]. It serves to calculate water volume limits to be respected in reservoirs set up in parallel on a hydrographic network, to allow the best future satisfaction of a common downstream management objective (low-water support or flood lamination).


## Requirements

VGEST has only been tested on Windows XP or superior.

* Download Free Pascal I386 for Windows 32 bits: https://www.freepascal.org/down/i386/win32.html
* For compilation in 64 bits, download the x86_64 cross compiler: https://www.freepascal.org/down/x86_64/win64.var
* Install it by running the downloaded executable

## Compilation

At the root folder of this repository type the following command line (Adapt the path of `fpc.exe` according to your Free Pascal installation path):

```
C:\FPC\bin\i386-win32\fpc.exe src\VGEST.PAS && move /Y src\VGEST.exe .
C:\FPC\bin\i386-win32\fpc.exe src\ENDE.PAS && move /Y src\ENDE.exe .
C:\FPC\bin\i386-win32\fpc.exe src\REGLAGE.PAS && move /Y src\REGLAGE.exe .
```

For 64 bits compilation, add `-Px86_64` parameter between `fpc.exe`  and the source file on each line.

For debugging, in case of a crash of the application, add the parameter `-gl` to the compilation command line in order to get the file and the line number where the crash occurs.

## Get started with the example

The `example` directory contains all the configuration for calculating statistics about respecting a maximum flow of 110 m<sup>3</sup>/s at Paris. These statistics are calculated from the Seine River Basin naturalised flow database between 1993/01/01 and 2009/12/31 [^Hydratec2011].

Copy the files contained in the `example` directory to the root directory and run `SURVGEST.bat`.

## Known issues

If a file `MODE.TXT` exists and it's content is different that "1", the software runs in interactive mode. In this mode the edition of the date of the study period is bugged.

## References

[^Bader2016]: Bader, J.-C., Dorchies, D., 2016. Calcul des limites de volumes d’eau à respecter dans des réservoirs implantés en parallèle sur un réseau hydrographique, pour permettre la meilleure satisfaction future d’un objectif commun de gestion à l’aval (soutien d’étiage ou laminage de crue) : logiciel VGEST - Application au cas du bassin de la Seine (Amélioration et extension de la méthode précédemment développée dans le cadre du programme Climaware). IRD, Montpellier. https://www.documentation.ird.fr/hor/fdi:010070461

[^Hydratec2011]: Hydratec, 2011. Actualisation de la base de données des débits journaliers naturalisés [Update of the naturalized daily streamflow database] (Rapport d’étude). Hydratec, Paris.
