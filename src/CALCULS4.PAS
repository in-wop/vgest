Unit CALCULS4;   {JC Bader, nov 2016}

interface
 uses Utilit,DECLARA;


procedure C4_TriChronique(var ZFic,ZFicT:YtypFod; ZTouComNul : double;
                       var ZQmoy,ZQmed,ZQmax,ZQmin : double; var ZQRare,ZQfrequent : YtypFreq);

IMPLEMENTATION

{=======================================================================}
{==Tri des valeurs d'une chronique contenue dans le fichier Zfic et   --}
{--�criture de ces valeurs tri�es dans le fichier ZFicT, puis calcule --}
{--les valeurs d�pass�es ou non d�pass�es pour certaines fr�quences   --}
{--pr�d�finies ainsi que min, max, m�diane et moyenne                 --}
{=======================================================================}

procedure C4_TriChronique(var ZFic,ZFicT:YtypFod; ZTouComNul : double;
                       var ZQmoy,ZQmed,ZQmax,ZQmin : double; var ZQRare,ZQfrequent : YtypFreq);

 var
  XRang        : longint;
  Xeffectif    : longint;
  XQ           : double;
  XQpred       : double;
  XStop        : boolean;
  Xi           : integer;
  Xfreq : double;

 begin
  reset(ZFic);
  rewrite(ZFicT);
  ZQmoy:=0;
  repeat
   read(ZFic,XQ);
   ZQmoy:=ZQmoy+XQ;
   if XQ>ZTouComNul then
    begin
     XStop:=false;
     XRang:=filesize(ZFicT)-1;
     seek(ZFicT,B_maxin(0,Xrang));
     while (XRang>=0) and (not Xstop) do
      begin
       read(ZFicT,XQpred);
       if XQPred>XQ then
        XStop:=true
       else
        begin
         write(ZFicT,XQPred);
         XRang:=XRang-1;
         seek(ZFicT,B_maxin(0,XRang));
        end;
      end;
     write(ZFicT,XQ);
    end;
  until eof(ZFic);
  close(ZFic);
  seek(ZFicT,filesize(ZFicT));
  reset(ZFicT);
  read(ZFicT,ZQmax);
  XEffectif:=filesize(ZFicT);
  seek(ZFicT,XEffectif-1);
  read(ZFicT,ZQmin);
  ZQmoy:=ZQmoy/XEffectif;
  for Xi:=1 to YNbFreq do
   begin
    XFreq:=1/YTpsFreq[Xi];
    XRang:=B_maxlongint(1,trunc((XEffectif+YB)*Xfreq+YA));
    seek(ZFicT,XRang-1);
    read(ZFicT,XQpred,XQ);
    ZQRare[Xi]:=XQPred+(XQ-XQpred)*(XFreq-(XRang-YA)/(XEffectif+YB))*(XEffectif+YB);
   end;
  XFreq:=0.5;
  XRang:=B_maxlongint(1,trunc((XEffectif+YB)*XFreq+YA));
  seek(ZFicT,XRang-1);
  read(ZFicT,XQpred,XQ);
  ZQmed:=XQPred+(XQ-XQpred)*(XFreq-(XRang-YA)/(XEffectif+YB))*(XEffectif+YB);
  for Xi:=1 to YNbFreq do
   begin
    XFreq:=1-1/YTpsFreq[YNbFreq+1-Xi];
    XRang:=B_minlongint(XEffectif-1,trunc((XEffectif+YB)*Xfreq+YA));
    seek(ZFicT,XRang-1);
    read(ZFicT,XQpred,XQ);
    ZQFrequent[Xi]:=XQPred+(XQ-XQpred)*(XFreq-(XRang-YA)/(XEffectif+YB))*(XEffectif+YB);
   end;
  close(ZFicT);
 end;

{=======================================================================}
BEGIN
END.
