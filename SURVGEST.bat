@echo off
if exist FiniFini del FiniFini
if exist PasOptim del PasOptim
if exist FinOptim del FinOptim
if exist PARAMETR\reglage.txt del PARAMETR\reglage.txt
if exist PARAMETR\OUITRI.TXT del PARAMETR\OUITRI.TXT
if exist Temporai.re\etape del Temporai.re\etape
if exist Temporai.re\result0 del Temporai.re\result0;
if exist Suivopti\suivi del Suivopti\suivi
if exist Temporai.re\dichot1 del Temporai.re\dichot1
if exist Temporai.re\dichot2 del Temporai.re\dichot2
if exist Temporai.re\dichot3 del Temporai.re\dichot3
if exist Temporai.re\dichot4 del Temporai.re\dichot4
:NouveauCalcul
REGLAGE
if exist PasOptim goto FindesCalculs
VGEST
if exist FinOptim goto FinDesCalculs
goto NouveauCalcul
:FinDesCalculs
del PARAMETR\reglage.txt
ENDE